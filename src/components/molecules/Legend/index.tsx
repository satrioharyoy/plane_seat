import React from "react";
import { Seat } from "../../atoms";

const Legend = () => {
  return (
    <div className="items-center my-4">
      <h3 className="text-lg font-semibold mb-5">Legend</h3>
      <div className="flex items-center mb-5">
        <Seat disableHover occupied={true} seatNumber="1A" selected={false} />
        <span className="text-lg ml-5">Occupied</span>
      </div>
      <div className="flex items-center mb-5">
        <Seat disableHover occupied={false} seatNumber="1A" selected={true} />
        <span className="text-lg ml-5">Selected</span>
      </div>
      <div className="flex items-center mb-5">
        <Seat disableHover occupied={false} seatNumber="1A" selected={false} />
        <span className="text-lg ml-5">Available</span>
      </div>
      <div className="flex items-center mb-5">
        <Seat
          disableHover
          occupied={false}
          seatNumber="Exit"
          selected={false}
          isExit
        />
        <span className="text-lg ml-5">Exit Door</span>
      </div>
    </div>
  );
};

export default Legend;
