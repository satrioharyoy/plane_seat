import React from "react";
import { SeatInfo } from "../../../types/seatTypes";

interface SeatProps {
  onClickCloseButton: (e: React.MouseEvent<HTMLButtonElement>) => void;
  seatInfo: SeatInfo;
  onClickBookSeat: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

const Seat: React.FC<SeatProps> = ({
  onClickCloseButton,
  seatInfo,
  onClickBookSeat,
}) => {
  return (
    <div className="absolute z-10 top-0 left-0 mt-12 ml-2 bg-white p-4 border border-gray-300 w-64 rounded shadow">
      <div className="flex justify-end mb-1">
        <button
          className="text-sm text-gray-500 hover:text-gray-700"
          onClick={onClickCloseButton}
        >
          close
        </button>
      </div>
      <div>
        <p className="mb-1">
          Seat Number:{" "}
          <span className="font-bold">{seatInfo?.seat_number}</span>
        </p>
        <p className="mb-1">
          Price: <span className="font-bold">{seatInfo?.price}</span>
        </p>
        <p className="mb-1">
          Class: <span className="font-bold">{seatInfo?.class}</span>
        </p>
      </div>
      {!seatInfo.occupied && (
        <button
          onClick={onClickBookSeat}
          className="w-full bg-blue-500 text-white font-semibold py-2 rounded mt-4 hover:bg-blue-600"
        >
          Book Seat
        </button>
      )}
    </div>
  );
};

export default Seat;
