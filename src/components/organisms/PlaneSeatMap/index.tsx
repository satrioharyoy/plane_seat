import React from "react";
import { SeatInfo } from "../../../types/seatTypes";
import { Gap, Seat } from "../../atoms";
import { PopupSeat } from "../../molecules";

interface PlaneSeatingMapProps {
  seatsData: SeatInfo[];
  cols: number;
  aisleIn: number;
  classTitle: string;
  setSeatSelected: (data: SeatInfo | null) => void;
  setShowPopup: (data: boolean) => void;
  showPopup: boolean;
  seatSelected: SeatInfo | null;
  aisleGap: string;
  onClickBookSeat: (seatId: string, occupied: boolean) => void;
}

const PlaneSeatingMap: React.FC<PlaneSeatingMapProps> = ({
  seatsData,
  cols,
  aisleIn,
  classTitle,
  setSeatSelected,
  setShowPopup,
  showPopup,
  seatSelected,
  aisleGap,
  onClickBookSeat,
}) => {
  const seatingMap = createPlaneSeatingMap(seatsData, cols);

  const selectSeat = (seatInfo: SeatInfo) => {
    setShowPopup(true);
    setSeatSelected(seatInfo);
  };

  const onClosePopup = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    setShowPopup(false);
    setSeatSelected(null);
  };

  const doClickBookSeat = (
    e: React.MouseEvent<HTMLButtonElement>,
    seatId: string,
    occupied: boolean
  ) => {
    e.stopPropagation();
    onClickBookSeat(seatId, occupied);
    setShowPopup(false);
    setSeatSelected(null);
  };

  function createPlaneSeatingMap(
    seatsData: PlaneSeatingMapProps["seatsData"],
    cols: number
  ): SeatInfo[][] {
    let seatingMap: SeatInfo[][] = [];
    let rows = Math.ceil(seatsData.length / cols);
    let seatCount = 0;

    for (let i = 0; i < rows; i++) {
      seatingMap.push([]);
      for (let j = 0; j < cols; j++) {
        if (seatsData[seatCount] !== undefined) {
          seatingMap[i].push(seatsData[seatCount]);
        }
        seatCount++;
      }
    }

    return seatingMap;
  }

  return (
    <div>
      <div className="flex justify-center w-full">
        <div className="flex py-5 items-center w-full">
          <div className="flex-grow border-t border-gray-400"></div>
          <span className="flex-shrink mx-4 text-gray-700">{classTitle}</span>
          <div className="flex-grow border-t border-gray-400"></div>
        </div>
      </div>
      <div className="flex items-center justify-between mb-10">
        <div className="flex justify-center items-center w-10 h-10 bg-gray-500">
          Exit
        </div>
        <div className="flex justify-center items-center w-10 h-10 bg-gray-500">
          Exit
        </div>
      </div>
      {seatingMap.map((row, rowIndex) => (
        <div key={rowIndex} className="flex justify-center mb-5 w-">
          {row.map((seat, colIndex) => (
            <React.Fragment key={colIndex}>
              <Seat
                occupied={seat.occupied}
                seatNumber={seat.seat_number}
                selected={seatSelected?.id === seat.id}
                onClickSelectSeat={() => selectSeat(seat)}
              >
                {showPopup && seatSelected?.id === seat.id && (
                  <PopupSeat
                    seatInfo={seatSelected}
                    onClickCloseButton={(
                      e: React.MouseEvent<HTMLButtonElement>
                    ) => onClosePopup(e)}
                    onClickBookSeat={(e: React.MouseEvent<HTMLButtonElement>) =>
                      doClickBookSeat(e, seat.id, true)
                    }
                  />
                )}
              </Seat>
              {((colIndex + 1) % aisleIn === 0 || colIndex + (1 % 3) === 0) &&
                colIndex !== row.length - 1 && <Gap width={aisleGap} />}
            </React.Fragment>
          ))}
        </div>
      ))}
      <div className="flex justify-between">
        <div className="flex justify-center items-center w-10 h-10 bg-gray-500">
          Exit
        </div>
        <div className="flex justify-center items-center w-10 h-10 bg-gray-500">
          Exit
        </div>
      </div>
    </div>
  );
};

export default PlaneSeatingMap;
