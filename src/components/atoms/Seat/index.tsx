import React, { ReactNode } from "react";

interface SeatProps {
  seatNumber: string;
  occupied: boolean;
  selected: boolean;
  onClickSelectSeat?: () => void;
  children?: ReactNode; // Tambahkan children sebagai ReactNode
  disableHover?: boolean;
  isExit?: boolean;
}

const Seat: React.FC<SeatProps> = ({
  seatNumber,
  occupied,
  selected,
  children,
  onClickSelectSeat,
  disableHover,
  isExit,
}) => {
  const getBackgroundColorSeat = () => {
    let bgColor = "bg-green-500";
    if (occupied && !selected) {
      bgColor = "bg-red-500";
    }
    if (!occupied && selected) {
      bgColor = "bg-yellow-400";
    }
    if (isExit) {
      bgColor = "bg-gray-500";
    }
    return bgColor;
  };
  return (
    <div
      className={`relative w-10 h-10 flex items-center justify-center m-1 ${
        disableHover ? "" : "hover:bg-yellow-400 hover:cursor-pointer"
      }   ${getBackgroundColorSeat()}`}
      onClick={onClickSelectSeat}
    >
      {seatNumber}
      {children}
    </div>
  );
};

export default Seat;
