import React from "react";

interface GapProps {
  height?: string;
  width?: string;
}

const Gap: React.FC<GapProps> = ({ height = "0", width = "0" }) => {
  return <div className={`w-20 h-${height}`} />;
};

export default Gap;
