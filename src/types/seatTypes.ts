export type SeatInfo = {
  id: string;
  class: string;
  seat_number: string;
  price: number;
  occupied: boolean;
};
