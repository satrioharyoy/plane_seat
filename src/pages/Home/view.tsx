import React from "react";
import { Gap, Legend, Loading, AlertMessage } from "../../components";
import PlaneSeatingMap from "../../components/organisms/PlaneSeatMap";
import { SeatInfo } from "../../types/seatTypes";

interface ViewProps {
  firstSeatsData: SeatInfo[];
  businessSeatsData: SeatInfo[];
  premiumEconomySeatsData: SeatInfo[];
  economySeatsData: SeatInfo[];
  showPopup: boolean;
  seatSelected: SeatInfo | null;
  setSeatSelected: (data: SeatInfo | null) => void;
  setShowPopup: (data: boolean) => void;
  isLoading: boolean;
  error: { isError: boolean; messageError: string };
  onCloseAlert: () => void;
  onClickBookSeat: (seatId: string, occupied: boolean) => void;
  success: { isSuccess: boolean; messageSuccess: string };
  onCloseAlertSuccess: () => void;
}

const View: React.FC<ViewProps> = ({
  firstSeatsData,
  businessSeatsData,
  premiumEconomySeatsData,
  economySeatsData,
  showPopup,
  seatSelected,
  setSeatSelected,
  setShowPopup,
  isLoading,
  error,
  onCloseAlert,
  onClickBookSeat,
  onCloseAlertSuccess,
  success,
}) => {
  return (
    <>
      <div className="flex h-screen">
        <aside className="w-1/4 h-full mx-10">
          <Legend />
        </aside>
        <article className="flex w-full justify-center h-full bg-gray-200 overflow-scroll">
          <div className="w-1/2">
            <PlaneSeatingMap
              classTitle="First Class"
              seatsData={firstSeatsData}
              cols={4}
              aisleIn={1}
              setSeatSelected={(data: SeatInfo | null) => {
                setSeatSelected(data);
              }}
              showPopup={showPopup}
              setShowPopup={(data: boolean) => {
                setShowPopup(data);
              }}
              seatSelected={seatSelected}
              aisleGap="24"
              onClickBookSeat={(seatId: string, occupied: boolean) =>
                onClickBookSeat(seatId, occupied)
              }
            />
            <PlaneSeatingMap
              classTitle="Business Class"
              seatsData={businessSeatsData}
              cols={6}
              aisleIn={2}
              setSeatSelected={(data: SeatInfo | null) => setSeatSelected(data)}
              showPopup={showPopup}
              setShowPopup={(data: boolean) => setShowPopup(data)}
              seatSelected={seatSelected}
              aisleGap="20"
              onClickBookSeat={(seatId: string, occupied: boolean) =>
                onClickBookSeat(seatId, occupied)
              }
            />
            <PlaneSeatingMap
              classTitle="Premium Class"
              seatsData={premiumEconomySeatsData}
              cols={9}
              aisleIn={3}
              setSeatSelected={(data: SeatInfo | null) => setSeatSelected(data)}
              showPopup={showPopup}
              setShowPopup={(data: boolean) => setShowPopup(data)}
              seatSelected={seatSelected}
              aisleGap="8"
              onClickBookSeat={(seatId: string, occupied: boolean) =>
                onClickBookSeat(seatId, occupied)
              }
            />
            <PlaneSeatingMap
              classTitle="Economy Class"
              seatsData={economySeatsData}
              cols={9}
              aisleIn={3}
              setSeatSelected={(data: SeatInfo | null) => setSeatSelected(data)}
              showPopup={showPopup}
              setShowPopup={(data: boolean) => setShowPopup(data)}
              seatSelected={seatSelected}
              aisleGap="8"
              onClickBookSeat={(seatId: string, occupied: boolean) =>
                onClickBookSeat(seatId, occupied)
              }
            />
            <Gap height="10" />
          </div>
        </article>
      </div>
      {isLoading && <Loading />}
      {error.isError && (
        <AlertMessage
          onCloseAlert={onCloseAlert}
          message={error.messageError}
          typeAlert="errorAlert"
        />
      )}
      {success.isSuccess && (
        <AlertMessage
          onCloseAlert={onCloseAlertSuccess}
          message={success.messageSuccess}
          typeAlert="successAlert"
        />
      )}
    </>
  );
};

export default View;
