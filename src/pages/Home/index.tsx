import { useEffect, useState } from "react";
import { getSeatData, updateBookseat } from "../../services/seat/actions";
import { SeatInfo } from "../../types/seatTypes";
import View from "./view";

function App() {
  const [showPopup, setShowPopup] = useState(false);
  const [seatSelected, setSeatSelected] = useState<SeatInfo | null>(null);
  const [isLoading, setLoading] = useState(false);
  const [firstSeatsData, setFirstSeatData] = useState<SeatInfo[]>([]);
  const [businessSeatsData, setBusinessSeatData] = useState<SeatInfo[]>([]);
  const [premiumEconomySeatsData, setPremiumSeatData] = useState<SeatInfo[]>(
    []
  );
  const [economySeatsData, setEconomySeatData] = useState<SeatInfo[]>([]);
  const [error, setError] = useState({ isError: false, messageError: "" });
  const [success, setSuccess] = useState({
    isSuccess: false,
    messageSuccess: "",
  });

  const fetchData = async () => {
    setLoading(true);
    const seatDataResponse = await getSeatData();
    if (seatDataResponse.isSuccess) {
      setFirstSeatData(seatDataResponse.firstSeats);
      setBusinessSeatData(seatDataResponse.businessSeats);
      setPremiumSeatData(seatDataResponse.premiumEconomySeats);
      setEconomySeatData(seatDataResponse.economySeats);
      setLoading(false);
    }
    if (seatDataResponse.isError) {
      setError({ isError: true, messageError: "Failed get seat data" });
      setLoading(false);
    }
  };

  const doBookseat = async (seatId: string, occupied: boolean) => {
    setLoading(true);
    setShowPopup(false);
    try {
      const dataResponse = await updateBookseat(seatId, occupied);
      if (dataResponse.isSuccess) {
        setSuccess({ isSuccess: true, messageSuccess: "Success booked seat" });
      }
    } catch (error) {
      setError({ isError: true, messageError: "Failed to book seat" });
    } finally {
      fetchData();
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <View
      firstSeatsData={firstSeatsData}
      businessSeatsData={businessSeatsData}
      premiumEconomySeatsData={premiumEconomySeatsData}
      economySeatsData={economySeatsData}
      showPopup={showPopup}
      setShowPopup={(data: boolean) => setShowPopup(data)}
      seatSelected={seatSelected}
      setSeatSelected={(data: SeatInfo | null) => setSeatSelected(data)}
      isLoading={isLoading}
      error={error}
      onCloseAlert={() => setError({ isError: false, messageError: "" })}
      onClickBookSeat={(seatId: string, occupied: boolean) =>
        doBookseat(seatId, occupied)
      }
      onCloseAlertSuccess={() =>
        setSuccess({ isSuccess: false, messageSuccess: "" })
      }
      success={success}
    />
  );
}

export default App;
