import { AxiosResponse } from "axios";
import { api } from "../../config";
import { SeatInfo } from "../../../types/seatTypes";
import { GET_SEAT_DATA, PUT_SEAT_DATA } from "../../url";

interface SeatDataResponse {
  firstSeats: SeatInfo[];
  businessSeats: SeatInfo[];
  premiumEconomySeats: SeatInfo[];
  economySeats: SeatInfo[];
  isSuccess: boolean;
  isError: boolean;
}

export const getSeatData = async (): Promise<SeatDataResponse> => {
  try {
    const response: AxiosResponse<SeatInfo[]> = await api.get(GET_SEAT_DATA);
    return {
      firstSeats: response.data
        .filter((seat: SeatInfo) => seat.class === "first")
        .sort((a, b) => {
          if (a.seat_number < b.seat_number) {
            return -1;
          }
          if (a.seat_number > b.seat_number) {
            return 1;
          }
          return 0;
        }),
      businessSeats: response.data
        .filter((seat: SeatInfo) => seat.class === "business")
        .sort((a, b) => {
          if (a.seat_number < b.seat_number) {
            return -1;
          }
          if (a.seat_number > b.seat_number) {
            return 1;
          }
          return 0;
        }),
      premiumEconomySeats: response.data
        .filter((seat: SeatInfo) => seat.class === "premium economy")
        .sort((a, b) => {
          if (a.seat_number < b.seat_number) {
            return -1;
          }
          if (a.seat_number > b.seat_number) {
            return 1;
          }
          return 0;
        }),
      economySeats: response.data
        .filter((seat: SeatInfo) => seat.class === "economy")
        .sort((a, b) => {
          if (a.seat_number < b.seat_number) {
            return -1;
          }
          if (a.seat_number > b.seat_number) {
            return 1;
          }
          return 0;
        }),
      isSuccess: true,
      isError: false,
    };
  } catch (error) {
    return {
      firstSeats: [],
      businessSeats: [],
      premiumEconomySeats: [],
      economySeats: [],
      isSuccess: false,
      isError: true,
    };
  }
};

export const updateBookseat = async (
  seatId: string,
  occupied: boolean
): Promise<{ isSuccess: boolean; isError: boolean }> => {
  try {
    const response: AxiosResponse<SeatInfo[]> = await api.put(
      `${PUT_SEAT_DATA}{${seatId}}`,
      { id: seatId, occupied: occupied }
    );
    let dataResponse = { isSuccess: false, isError: false };
    if (response.status === 204) {
      dataResponse.isSuccess = true;
    }
    return dataResponse;
  } catch (error) {
    return {
      isSuccess: false,
      isError: true,
    };
  }
};
