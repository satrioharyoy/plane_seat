const BASE_URL = "https://otqzegjvkqnmtiahmrvx.supabase.co/rest/v1";
const API_KEY =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im90cXplZ2p2a3FubXRpYWhtcnZ4Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzYzMTI0NTcsImV4cCI6MTk5MTg4ODQ1N30.BfS07hDkckwmtdliv0M4HoSDJC0hTsebH8RBIYpCNaw";
const GET_SEAT_DATA = "seats";
const PUT_SEAT_DATA = "seats?id=eq.";

export { BASE_URL, GET_SEAT_DATA, API_KEY, PUT_SEAT_DATA };
